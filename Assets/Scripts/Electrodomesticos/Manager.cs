using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DatosProductos;

public class Manager : MonoBehaviour
{ 
    //ELECTRODOMESTICOS (Preparacion de alimentos)
    public TextMeshProUGUI txtPc_List;
    public TextMeshProUGUI txtPl_List;

    List<Electrodomesticos> pcObj = new List<Electrodomesticos>();
    List<Electrodomesticos> plObj = new List<Electrodomesticos>();

    // Start is called before the first frame update
    void Start()
    {
        txtPc_List.text = string.Empty;
        txtPl_List.text = string.Empty;
    }

    //Para Comidas
    public void ProcesadorDeAlimentos()
    {
        pcObj.Add(new Electrodomesticos("Procesador de alimentos", "Bosch", 3.65f));
        UpdateParaComidaLista();
    }

    public void Freidora()
    {
        pcObj.Add(new Electrodomesticos("Freidora", "Philips", 20f));
        UpdateParaComidaLista();
    }


    //Para Liquidos
    public void Licuadora()
    {
        plObj.Add(new Electrodomesticos("Licuadora", "Oster", 3.45f));
        UpdateParaLiquidosLista();
    }

    public void Cafetera()
    {
        plObj.Add(new Electrodomesticos("Cafetera", "Moka", 0.750f));
        UpdateParaLiquidosLista();
    }


    //Updates de listas
    private void UpdateParaComidaLista()
    {
        txtPc_List.text = string.Empty;

        for (int i = 0; i < pcObj.Count; i++)
        {
            txtPc_List.text += (pcObj[i].nombre + " - " + pcObj[i].marca + " - peso: " + pcObj[i].peso) + "\n";
        }
    }

    private void UpdateParaLiquidosLista()
    {
        txtPl_List.text = string.Empty;

        for (int i = 0; i < plObj.Count; i++)
        {
            txtPl_List.text += (plObj[i].nombre + " - " + plObj[i].marca + " - peso: " + plObj[i].peso) + "\n";
        }
    }


}
