using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosProductos
{
    class Electrodomesticos
    {
        string _nombre;
        public string nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                if (value.Length > 4)
                    _nombre = value;
            }
        }

        string _marca;
        public string marca
        {
            get
            {
                return _marca;
            }
            set
            {
                if (value.Length > 1)
                    _marca = value;
            }
        }

        float _peso;
        public float peso
        {
            get
            {
                return _peso;
            }
            set
            {
                if (value > 0)
                    _peso = value;
            }
        }

        public Electrodomesticos(string nombre, string marca, float peso)
        {
            this.nombre = nombre;
            this.marca = marca;
            this.peso = peso;
        }



    }
}