using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : Example
{
    public override void Start()//polimorfismo
    { Debug.Log("iniciar antes"); base.Start(); }
} //comienza el start del inicio

public class Example : MonoBehaviour
{
    public virtual void Start()
    {
        Programmer programmer = new Programmer();
        programmer.leg(5);
    }
}
public class Programmer //contenido
{
    public Programmer()
    {
        Debug.Log("Que miras");
    }
    public void leg(float valordelparametro)
    {
        Debug.Log($"{valordelparametro + 1} sube");
    }

}
